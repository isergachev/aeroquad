#!/bin/sh
export PATH=~/code/gcc-arm-none-eabi-4_7-2013q2/bin:$PATH
cd Libmaple/libmaple
make clean
make -j all
cd ../maple-bootloader
make clean
make -j all
cd ../../BuildAQ32
make clean
make -j all
