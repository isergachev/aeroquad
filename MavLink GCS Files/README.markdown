Note
----------------------------------------
This directory contains (optional) additional files for ground control stations which allow communication via MavLink. They add support for modifying and executing AeroQuad specific parameters and functions. 

TODO: After official release create more detailed wiki article / how-to

**files.rar**

This file is intended for use with QGroundControl v1.1.0 or higher. Unzip the content of the "files.rar" file directly into the QGroundControl program folder and overwrite all files if prompted.
Afterwards restart QGroundControl and connect with the AeroQuad (via MavLink). Under the "Config" tab you will now notice that the "Sensor Calibration" and "General Config" menues
are now filled with specific parameters and functions for AeroQuad.